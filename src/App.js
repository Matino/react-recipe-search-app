import React, { Component } from 'react';
import './App.css';
import Form from './components/Form';
import Recipes from './components/Recipes';

const API_KEY = "11ffc105e298f3ce31638c91384f5ced";


class App extends Component {
  state = {
    recipes: []
  }

  getRecipe = async (e) => {
    e.preventDefault();
    const recipeName = e.target.elements.recipeName.value//to retrieve the value from the input
    // console.log(recipeName);
    const api_call = await fetch(`https://www.food2fork.com/api/search?key=${API_KEY}&q=${recipeName}&count=10`);
    const data = await api_call.json();
    // console.log(data.recipes[0].recipe_id);
    this.setState ({recipes: data.recipes});//to feed in data to the recipes state
    // console.log(this.state.recipes);//to print out the contents of the state
    console.log(data);
  }
  //method that defines whatever hapens in it is as a result of updating the 'App' component ie. when the state changes
  componentDidUpdate = () => {
    const recipes = JSON.stringify(this.state.recipes);
    localStorage.setItem("recipes", recipes);
  }

  componentDidMount= () => {
    const json = localStorage.getItem("recipes");
    const recipes = JSON.parse(json);
    this.setState({ recipes });

  }
  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Recipe Search</h1>
        </header>
        <Form getRecipe = {this.getRecipe}/>
        <Recipes recipes = {this.state.recipes}/>
      </div>
    );
  }
}

export default App;
