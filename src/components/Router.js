import React from 'react';
import {BrowserRouter,Switch, Route} from 'react-router-dom';
import App from '../App';
import Recipe from './Recipe';



const Router = ()=> (
  <BrowserRouter>
    <Switch>
      <Route exact path = "/" component = {App}/>
      {/* to create a router parameter, add :. Anything after the : is a variable */}
      <Route exact path = "/recipe/:id" component = {Recipe}/>
    </Switch>
  </BrowserRouter>
)
export default Router;
