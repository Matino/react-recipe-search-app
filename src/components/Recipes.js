import React from 'react';
import { Link } from 'react-router-dom';

const Recipes = props => (
  <div className = "container">
    <div className="row">
      {/* for each loop: */}
      {props.recipes.map((recipe) => {
        return (
          <div className="col-md-4" key = {recipe.recipe_id} style = {{ marginBottom: "2rem" }}>
            <div className="recipes__box">
              <img
                className = "recipe__box"
                src={recipe.image_url}
                alt={recipe.title}/>
                <div className="recipe__text">
                  <h5 className = "recipes__title">
                    {/* to trim the title to a length that can be displayed uniformly */}
                    { recipe.title.length < 20 ? `${recipe.title}` : `${recipe.title.substring(0,25)}...`}
                  </h5>
                  <p className = "recipes__subtitle">Publisher: <span>
                    {recipe.publisher}
                  </span></p>
                  <button className = "recipe_buttons">
                    <Link to = {{
                      pathname: `/recipe/${recipe.recipe_id}`,
                      state: { recipe: recipe.title }
                     }}>View Recipe</Link>
                  </button>
                </div>
            </div>
          </div>
        );
      })}
    </div>



  </div>
)




export default Recipes;
