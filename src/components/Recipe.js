import React, {Component} from 'react';
import {Link} from 'react-router-dom';

const API_KEY = "11ffc105e298f3ce31638c91384f5ced";

class Recipe extends Component{
  //console.log(this.props);
  state = {
    activeRecipe: []
  }
  // method that is going to be fired as soon as the "Recipe" component is mounted onto the web browser.
  componentDidMount = async() => {

    const title = this.props.location.state.recipe;
    const req = await fetch(`https://www.food2fork.com/api/search?key=${API_KEY}&q=${title}`);
    const res = await req.json();
    // console.log(res.recipes[0]);
    this.setState({ activeRecipe : res.recipes[0] });
    console.log(this.state.activeRecipe);

  }
  render(){
    const recipe = this.state.activeRecipe;

    return(
      <div className="container">
        { this.state.activeRecipe.length !== 0 &&
          <div className="active-recipe">
            <img className="active-recipe__img"  src={ recipe.image_url } alt={ recipe.title }/>
            <h3 className="active-recipe__title">{ recipe.title }</h3>
            <h4 className="active-recipe__publisher">
              Publisher: <span>{ recipe.publisher }</span>
            </h4>
            <p className="active-recipe__website">
              Website: <span><a href = "{ recipe.publisher_url}">{ recipe.publisher_url}</a></span>
            </p>
            <button className="active-recipe__button">
              <Link to ='/'>Go Home</Link>
            </button>

          </div>
         }
        </div>
    );

  }
}
export default Recipe;
